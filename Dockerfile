FROM alpine:latest
LABEL maintainer="Manion Group DevOps Team <devops@mansion.com>"

ENV TERRAFORM_DOCS_VERSION="v0.16.0"

RUN apk --no-cache add \
    jq \
    curl \
    git \
    wget \
    openssh

RUN curl -sLo terraform_docs.tar.gz "https://github.com/terraform-docs/terraform-docs/releases/download/${TERRAFORM_DOCS_VERSION}/terraform-docs-${TERRAFORM_DOCS_VERSION}-linux-amd64.tar.gz" && \
    tar -xf terraform_docs.tar.gz && \
    mv terraform-docs /usr/bin/terraform-docs && \
    chmod +x /usr/bin/terraform-docs && \
    rm -rf terraform_docs.tar.gz
    
ENTRYPOINT []
